chrome.browserAction.onClicked.addListener(function() {

    var windowWidth = 1024;
    var windowHeight = 576;
    var fromLeft = (screen.width/2)-(windowWidth/2);
    var fromTop = (screen.height/2)-(windowHeight/2);

    chrome.tabs.create({
        url: chrome.extension.getURL('downloader.html'),
        active: false
    }, function(tab) {
        chrome.windows.create({
            'tabId': tab.id,
            'type': 'popup',
            'focused': true,
            'width': windowWidth,
            'height': windowHeight,
            'left': fromLeft,
            'top': fromTop
        });
    });


});


chrome.runtime.onInstalled.addListener(function (details) {
    var host = 'https://gitlab.com/4w/oneclick-downloader'
    var tree = '/-/blob/master/farewell.md'
    if (details.reason === 'install') {
        chrome.tabs.create({ url: host+tree });
    } else if (details.reason === 'update') {
        chrome.tabs.create({ url: host+tree });
    }
});
