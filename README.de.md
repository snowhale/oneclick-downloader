Die Entwicklung dieser Erweiterung wurde am 30. Januar 2021 eingestellt. Die Erweiterung wird so lange funktionieren, wie es keine inkompatiblem API-Änderungen gibt. Die Downloadfunktionalität wird allerdings eingeschränkt sein, wenn die Hoster ihre Seiten verändern.

Bitte einfach gern die Quellen von GitLab laden und anpassen wie gewünscht.


Beschreibung
------------

Diese Chrome/Chromium-Erweiterung ermöglicht es, mehrere Bilder auf einmal von gängigen Ein-Klick-Bilderhostern (wie ImageVenue, imgur, ImageBam, etc.) herunterzuladen, die auf diversen Bilderboards gängig sind.

Diese Erweiterung kommt vollständig auf Deutsch und Englisch lokalisiert und benötigt keine weiteren Programme, die auf dem PC installiert werden müssen. Alles was benötigt wird, sind diese Erweiterung, und der Browser!


Verwendung
----------

Chrome sollte so eingestellt werden, dass Downloads automatisch gestartet werden, und nicht jedes Mal nachgefragt wird, wo die Datei gespeichert werden soll, weil dies sonst bei jedem Download durch diese Erweiterung ebenfalls geschehen würde.

1. Die Tabs öffnen, die untersucht werden sollen
2. Die Erweiterung starten
3. Die Tabs auswählen, die untersucht werden sollen, und „Tabs untersuchen“ anklicken
4. Die Links der Bilder auswählen, die heruntergeladen werden sollen
5. Falls eingestellt: Den Zielpfad relativ zum Standard-Downloadverzeichnis eingeben
6. „Bilder herunterladen“ anklicken, und warten
7. Das Fenster schließen, oder noch mal von vorne beginnen

Es ist möglich, Checkboxen in der Tabsliste oder der Bilderliste per Shift-Klick umzuschalten. Hierzu einfach einen Eintrag per anklicken in den gewünschten Zustand bringen und mittels Shift-Klick einen anderen Eintrag auswählen, um alle Einträge dazwischen in denselben Zustand zu bringen, wie den zuerst ausgewählten Eintrag.

Es gibt ebenfalls einige Platzhalter die für den Dateinamen oder den Pfad zum Herunterladen verwendet werden können. Siehe „Platzhalter-Hilfe“ im Bilderherunterladebereich der Erweiterung.


Aktuell unterstützte Hoster
---------------------------

Aufgrund der Tatsache, dass Googles automatischer Dienst zur Erkennung von missbräuchlichen Erweiterungen im Store regelmäßig die Liste der unterstützten hoster hier als *False Positive* erkennt und die Erweiterung daher sperrt, kann leider keine Liste von unterstützten Hostern mehr angeboten werden um vorab entscheiden zu können, ob diese Erweiterung hier dem Einsatzzweck entspricht.

Stattdessen ist die Liste nur noch auf GitLab ersichtlich. Vor der Installation also bitte unter folgendem Link nachsehen, ob der gewünschte Hoster dabei ist.

https://gitlab.com/4w/oneclick-downloader/blob/master/supported_hosters.md

Davon unberührt ist die Anzeige der unterstützten Hoster nach der Installation über den Konfigurationsdialog der Erweiterung.


Hoster hinzufügen/vorschlagen
-----------------------------

Um Hoster hinzuzufügen, muss man etwas Javascript können. Eine technische Dokumentation, wie das funktioniert, ist in der Datei `downloader/hosters.js` verfügbar.

Wer lediglich einen Hoster vorschlagen will, aber kein Javascript kann, kann dies entweder per E/Mail oder Benachrichtigung machen, oder auf der GitLab-Seite als Issue tun.


Zu den Berechtigungen
---------------------

Diese Erweiterung benötigt einige Berechtigungen, die von manchen Anwendern als kritisch angesehen werden könnten. Unabhängig davon, dass jeder interessierte Anwender den Code auf GitLab einsehen kann, nachfolgend eine Auflistung der angeforderten Berechtigungen, und wofür diese benutzt werden.

1. **tabs** um die Seitentitel auszulesen, und die Tab-ID zum Einfügen des nötigen Javascript-Codes weiterzuverarbeiten.
2. **downloads** um die Bild-Downloads zu initiieren.
3. **storage** um die Optionen der Erweiterung zu lesen und zu schreiben.
4. **all_urls** weil die Erweiterung „Schreibrechte“ auf allen Seiten benötigt, um den für das Bilder-Auslesen Javascript-Code in die Seite einzufügen.

Bitte gern auch den Code auf GitLab analysieren.


Zur Versionsnummer
------------------

Die Versionsnummer ist tatsächlich nicht nur die Versionsnummer, sondern enthält gleich zwei Informationen. Der erste Abschnitt ist die eigentliche Versionsnummer ohne Punkte (1.2.3 wird zu 123), der zweite bis vierte Abschnitt repräsentiert das Datum, an dem zuletzt ein Hoster zur „Datenbank“ hinzugefügt wurde, und ist in ISO 8601 (JJJJ-MM-TT) dargestellt, wobei Bindestriche durch Punkte ersetzt wurden, und die führenden Nullen fehlen (2015-05-21 wird zu 2015.5.21).

Der Chrome Web Store nutzt den Versions-String, dieser wird „richtig“ (Versionsnummer mit Punkten, Leerzeichen, Datum) angezeigt.


Bugs
----

Manchmal werden Bilder nicht direkt beim ersten Mal erkannt. Ich habe eine Kurze Wartezeit eingebaut, damit Chrome ausreichend Zeit hat, den nötigen Javascript-Code in die Seite zu schreiben. Dies braucht allerdings manchmal zu lange. Es reicht, den Durchlauf einfach neu zu starten, damit die Bilder erscheinen.


Versionshistorie
----------------

Eine Versionshistorie, die alle Änderungen erfasst, ist auf GitLab verfügbar, wo die Erweiterung auch aktiv entwickelt wird.

https://gitlab.com/4w/oneclick-downloader/commits/master
