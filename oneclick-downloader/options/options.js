document.addEventListener('DOMContentLoaded', function() {
    document.title = t('optionsTitle', true);
});


function getOption(name) {
    var options = document.getElementsByName(name);
    for (var i = 0; i < options.length; i++) {
        if (options[i].checked)
            return options[i].value;
    }
}

function setOption(name, inputValue) {
    var options = document.getElementsByName(name);
    for (var i = 0; i < options.length; i++) {
        if (options[i].value == inputValue)
            options[i].checked = true;
    }
}


function saveOptions() {

    // Get Path-related options
    var downloadPathOption = getOption('downloadPathOption');
    var downloadCustomPath =
        document.getElementById('downloadCustomPath').value;

    // Get startup-related options
    var startupBehavior = getOption('startupBehavior');

    // Get schema related options
    var filenameSchema = document.getElementById('filenameSchema').value;

    chrome.storage.sync.set({

        // Save options
        'downloadPathOption': downloadPathOption,
        'downloadCustomPath': downloadCustomPath,
        'startupBehavior': startupBehavior,
        'filenameSchema': filenameSchema

    }, showInfo);
}



function showInfo(text, timeout) {
    var s = document.getElementById('status');
    s.style.opacity = 1.0;

    if (typeof text !== 'undefined') {
        s.textContent = text;
    } else {
        s.textContent = t('optionsInfoSaved', true);
    }
    if (typeof timeout === 'undefined') {
        timeout = 1500;
    }


    setTimeout(function() { s.style.opacity = 0; }, timeout);
}



function restoreOptions() {
    chrome.storage.sync.get({

        // Load options (set default values)
        'downloadPathOption': 'none',
        'downloadCustomPath': '',
        'startupBehavior': 'default',
        'filenameSchema': '%full%'

    }, function(items) {

        // Restore options
        setOption('downloadPathOption', items.downloadPathOption);

        document.getElementById('downloadCustomPath').value =
            items.downloadCustomPath

        setOption('startupBehavior', items.startupBehavior);

        document.getElementById('filenameSchema').value =
            items.filenameSchema;

    });
}



function resetOptions() {
    setOption('downloadPathOption', 'none');
    document.getElementById('downloadCustomPath').value = '';
    setOption('startupBehavior', 'default');
    document.getElementById('filenameSchema').value = '%full%';
}



document.addEventListener('DOMContentLoaded', function() {
    restoreOptions();
    document.getElementById('optionsSave').addEventListener('click',
            saveOptions);
    document.getElementById('optionsReset').addEventListener('click',
            resetOptions);

    listHostersButton = document.getElementById('listHostersButton');
    listHostersButton.addEventListener('click', function() {
        location.href = 'list-hosters.html';
    });

    customHostersButton = document.getElementById('customHostersButton');
    customHostersButton.addEventListener('click', function() {
        location.href = 'custom-hosters.html';
    });

});

