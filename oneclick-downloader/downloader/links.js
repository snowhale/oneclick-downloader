function getLinks() {
    var tabs = document.querySelectorAll('input.tabInfo:checked');


    for (var i=0; i < tabs.length; i++) {
        var id = parseInt(tabs[i].value);

        chrome.tabs.executeScript(id, {'file': 'downloader/injection.js'});

        var messageObject = {
            'action': 'getLinks',
            'tabID': id,
            'hosters': supportedHosters
        };

        // sleep() simulator for possible race condition of the above
        // chrome.tabs.executeScript() and the below ….sendMessage(), this
        // delays the execution for each tab by 500ms.
        var sleep = new Date().getTime();
        while (new Date().getTime() < sleep + 500);

        chrome.tabs.sendMessage(id, messageObject, function(result) {
            if (result.links !== undefined) {
                var links = result.links;
            } else {
                var links = [];
            }

            var linksList = document.getElementById('linksList');

            for (var i=0; i < links.length; i++) {
                var link = links[i];
                var paddingZeros = links.length.toString().length
                var order = link.order.toString().padStart(paddingZeros, '0');
                var btoa = link.btoa
                var title = result.tabTitle.replace(/["']/g, "");
                var l = '<label>'+
                    '<input '+
                    'type="checkbox" '+
                    'value="'+link.href+'" '+
                    'class="linkInfo" '+
                    'data-tabid="'+result.tabID+'" '+
                    'data-tabtitle="'+title+'" '+
                    'data-order="'+order+'" '+
                    'data-btoa="'+btoa+'">'+
                    '<span>'+link.href+'</span>'+
                    '</label>';
                linksList.innerHTML += l;
            };

            var loadingLinks = document.getElementById('loadingLinks');
            loadingLinks.style.display = 'none';
            shiftSelect(linksList);
        });
    }


    linksCheckAll = document.getElementById('linksCheckAll');
    linksCheckAll.addEventListener('click', function () {
        var cbs = document.getElementsByClassName('linkInfo');
        for(var i=0; i < cbs.length; i++) {
            if(cbs[i].type == 'checkbox') {
                cbs[i].checked = true;
            }
        }
    });

    linksUnheckAll = document.getElementById('linksUncheckAll');
    linksUncheckAll.addEventListener('click', function () {
        var cbs = document.getElementsByClassName('linkInfo');
        for(var i=0; i < cbs.length; i++) {
            if(cbs[i].type == 'checkbox') {
                cbs[i].checked = false;
            }
        }
    });

    downloaderRestart = document.getElementById('downloaderRestart');
    downloaderRestart.addEventListener('click', function () {
        location.reload();
    });

    linksDownload = document.getElementById('linksDownload');
    linksDownload.addEventListener('click', function () {
        document.getElementById('scrape').style.display = 'none';
        document.getElementById('download').style.display = 'block';
        document.getElementById('appHeader').innerHTML =
            t('downloadHeader', true);
        downloadImages()
    });


}
